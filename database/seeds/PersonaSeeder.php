<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personas')->insert([
            'tipoDocumento'=>'CC',
            'numeroDocumento'=>'10100',
            'primerNombre'=>'Felipe',
            'segundoNombre'=>'Andres',
            'primerApellido'=>'Martinez',
            'segundoApellido'=>'Delgado',
            'email'=>'anfemardel@gmail.com',
            'direccion'=>'calle1'
        ]);
    }
}
