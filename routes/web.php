<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','PersonaController@index')->name('inicio');
Route::post('/agregar','PersonaController@store')->name('store');
Route::get('/agg', function () {
    return view('agregar');
});
Route::get('/editar/{id}','PersonaController@edit')->name('editar');
Route::put('/update/{id}','PersonaController@update')->name('update');
Route::delete('/eliminar/{id}','PersonaController@destroy')->name('eliminar');