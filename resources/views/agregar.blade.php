@extends('layouts.app')

@section('content')
<div class="content center">
    <h3 class="text-center mb-4">Agregar Usuario</h3>

    <form action="{{ route('store')}}" method="POST">
        @csrf
        <div class="form-group">
            <select class="custom-select custom-select-sm form-control" name="tipoDocumento" id="tipoDocumento" placeholder="tipoDocumento">
                
                <option value="CC">CC</option>
                <option value="TI">TI</option>
                <option value="CE">CE</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" name="numeroDocumento" id="numeroDocumento" class="form-control" placeholder="numeroDocumento" required>
        </div>
        <div class="form-group">
            <input type="text" name="primerNombre" id="primerNombre" class="form-control" placeholder="primerNombre" required>
        </div>
        <div class="form-group">
            <input type="text" name="segundoNombre" id="segundoNombre" class="form-segundoNombre" placeholder="segundoNombre" required>
        </div>
        <div class="form-group">
            <input type="text" name="primerApellido" id="primerApellido" class="form-control" placeholder="primerApellido" required>
        </div>
        <div class="form-group">
            <input type="text" name="segundoApellido" id="segundoApellido" class="form-control" placeholder="segundoApellido" required>
        </div>
        <div class="form-group">
            <input type="text" name="email" id="email" class="form-control" placeholder="email" required>
        </div>
        <div class="form-group">
            <input type="text" name="direccion" id="direccion" class="form-control" placeholder="direccion" required>
        </div>
    
        <button type="submit" class="btn btn-success btn-block">Guardar</button>
    </form>

    @if (session('agregar'))
        <div class="alert alert-success mt-3">
            {{ session('agregar')}}
        </div>
    @endif
</div>
@endsection