@extends('welcome')
@section('content')
    <h3 class="text-center mb-3 pt-3">Editar  {{$persona->primerNombre}}</h3>
    
    <form action="{{ route('update', $persona->id)}}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group">
            <select class="custom-select custom-select-sm form-control" name="tipoDocumento" id="tipoDocumento">
               
                <option value="CC">CC</option>
                <option value="TI">TI</option>
                <option value="CE">CE</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" name="numeroDocumento" id="numeroDocumento" class="form-control" value="{{$persona->numeroDocumento}}" required>
        </div>
        <div class="form-group">
            <input type="text" name="primerNombre" id="primerNombre" class="form-control" value="{{$persona->primerNombre}}" required>
        </div>
        <div class="form-group">
            <input type="text" name="segundoNombre" id="segundoNombre" class="form-segundoNombre" value="{{$persona->segundoNombre}}" required>
        </div>
        <div class="form-group">
            <input type="text" name="primerApellido" id="primerApellido" class="form-control" value="{{$persona->primerApellido}}" required>
        </div>
        <div class="form-group">
            <input type="text" name="segundoApellido" id="segundoApellido" class="form-control" value="{{$persona->segundoApellido}}" required>
        </div>
        <div class="form-group">
            <input type="text" name="email" id="email" class="form-control" value="{{$persona->email}}" required>
        </div>
        <div class="form-group">
            <input type="text" name="direccion" id="direccion" class="form-control" value="{{$persona->direccion}}" required>
        </div>

        <button type="submit" class="btn btn-success btn-block">Editar</button>
    </form>
    @if (session('update'))
        <div class="alert alert-success mt-3">
            {{ session('update')}}
        </div>
    @endif
@endsection