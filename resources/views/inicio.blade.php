@extends('layouts.app')

@section('content')
<a href="/agg" class="btn btn-warning">Agregar</a>
<div class="content">

    <div class="content center">
        <table class="table">
            <tr>
                <th>TD</th>
                <th>#</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>EMAIL</th>
                <th>&nbsp;</th>
            </tr>
            @foreach ($personas as $persona)
                <tr>
                    <td>{{$persona->tipoDocumento}}</td>
                    <td>{{$persona->numeroDocumento}}</td>
                    <td>{{$persona->primerNombre}}</td>
                    <td>{{$persona->primerApellido}}</td>
                    <td>{{$persona->email}}</td>
                    <td>
                        <a href="{{route('editar', $persona->id)}}" class="btn btn-warning">Editar</a>
                            <form action="{{ route('eliminar' , $persona->id)}}" method="POST" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger" redirect>Eliminar</button>
                            </form>
                    </td>
                </tr>
            @endforeach
        </table> 
        @if (session('eliminar'))
                <div class="alert alert-success mt-3">
                    {{ session('eliminar')}}
                </div>
            @endif 
        {{$personas->links()}}
    </div>

</div>
@endsection
