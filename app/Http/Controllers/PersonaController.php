<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Http\Requests\PersonaRequest;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = Persona::paginate(5);
        return view('inicio', compact('personas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonaRequest $request)
    {
        $persona = new Persona;
        $persona->tipoDocumento = $request->tipoDocumento;
        $persona->numeroDocumento = $request->numeroDocumento;
        $persona->primerNombre = $request->primerNombre;
        $persona->segundoNombre = $request->segundoNombre;
        $persona->primerApellido = $request->primerApellido;
        $persona->segundoApellido = $request->segundoApellido;
        $persona->email = $request->email;
        $persona->direccion = $request->direccion;
        
        $persona->save();
        return back()->with('agregar', 'Se agrego el Usuario');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function show(Persona $persona)
    {
        return view('editar');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $persona = Persona::findOrFail($id);
        return view('editar', compact('persona'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function update(PersonaRequest $request, $id)
    {
        $persona = Persona::findOrFail($id);
        $persona->tipoDocumento = $request->tipoDocumento;
        $persona->numeroDocumento = $request->numeroDocumento;
        $persona->primerNombre = $request->primerNombre;
        $persona->segundoNombre = $request->segundoNombre;
        $persona->primerApellido = $request->primerApellido;
        $persona->segundoApellido = $request->segundoApellido;
        $persona->email = $request->email;
        $persona->direccion = $request->direccion;
        
        $persona->save();
        return back()->with('agregar', 'Se Actualizo el Usuario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $persona = Persona::findOrFail($id);
        $persona->delete();
        return back()->with('eliminar', 'Se Elimino el usuario');
    }
}
